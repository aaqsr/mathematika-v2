import "bootstrap/dist/css/bootstrap.min.css";
import "jquery/dist/jquery.min.js";
import "popper.js/dist/popper.min";
import "bootstrap/dist/js/bootstrap.min.js";

import "./src/css/flex-slider.css";
import "./src/css/font-awesome.css";
import "./src/css/lightbox.css";
import "./src/css/templatemo-breezed.css";
import "./src/css/custom.css";

//import "./assets/js/animation.js";
//import "./assets/js/custom.js";
//import "./assets/js/imgfix.min.js";
//import "./assets/js/isotope.js";
//import "./assets/js/lightbox.js";
//import "./assets/js/scrollreveal.min.js";
//import "./assets/js/slick.js";
//import "./assets/js/waypoints.min.js";