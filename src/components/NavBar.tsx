import * as React from 'react';
import { Link } from 'gatsby';

export const NavBar = () => (
    <header className="header-area header-sticky">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <nav className="main-nav">
                        <a href="index.html" className="logo">
                            MATHEMATIKA
                        </a>
                        <ul className="nav">
                            <li className="scroll-to-section"><a href="#" className="active">Home</a></li>
                            <li className="scroll-to-section"><a href="index.html?#subscribe">Register</a></li>
                            <li><a href="talks.html">Talks</a></li>
                            <li><a href="comingsoon.html">Lectures</a></li>
                            <li><a href="comingsoon.html">Competitions</a></li>
                        </ul>
                        <a className='menu-trigger'>
                            <span>Menu</span>
                        </a>
                    </nav>
                </div>
            </div>
        </div>
    </header>
 
)