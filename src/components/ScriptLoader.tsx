import * as React from 'react'

export const ScriptLoader = () => (
    <div>
            <script src="assets/js/owl-carousel.js"></script>
            <script src="assets/js/scrollreveal.min.js"></script>
            <script src="assets/js/waypoints.min.js"></script>
            <script src="assets/js/jquery.counterup.min.js"></script>
            <script src="assets/js/imgfix.min.js"></script>
            <script src="assets/js/slick.js"></script>
            <script src="assets/js/lightbox.js"></script>
            <script src="assets/js/isotope.js"></script>

                <script src="assets/js/custom.js"></script>

                    <script src="https://cdn.jsdelivr.net/npm/typeit@7.0.4/dist/typeit.min.js"></script>
        </div>
)
