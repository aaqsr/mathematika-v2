import * as React from 'react'
import { NavBar } from '../components/NavBar';
import { ScriptLoader } from '../components/ScriptLoader';

const IndexPage = () => (
    <div>
    <NavBar />

    <ScriptLoader />
    </div>
)


export default IndexPage